#!/usr/bin/python3

import getpass
from datetime import datetime
from tkinter import *
from tkinter.messagebox import showinfo
import subprocess

class ToolTip(object):

    def __init__(self, widget):
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0


    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 57
        y = y + cy + self.widget.winfo_rooty() +27
        self.tipwindow = tw = Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(tw, text=self.text, justify=LEFT,
                      background="#ffffe0", relief=SOLID, borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()
def CreateToolTip(widget, text):
    toolTip = ToolTip(widget)
    def enter(event):
        toolTip.showtip(text)
    def leave(event):
        toolTip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)

date2 = datetime.today()
bugun = date2.strftime("%d_%m_%Y")
dosya_adi = f'hc_{bugun}.log'
k_adi = getpass.getuser()
yol = f'/home/{k_adi}/gorevLog/{dosya_adi}'
pencere= Tk()

pencere.resizable(width=False, height=False)
pencere.geometry('400x250')
pencere.configure(bg='SlateGray1')



def al():
    dosya = open(yol, 'a')
    date = datetime.now()
    simdi = date.strftime("%H:%M:%S")
    bildiri = metin.get(1.0,END)
    dosya.write(f'\n{simdi}: ')
    for i in bildiri:
        sifreli = str(ord(i))+' '
        dosya.write(sifreli)
    metin.delete('1.0', END)
    showinfo("Başarılı!", "Kayıt Eklendi.")
    dosya = open(yol, 'r')
    line_count = 0
    for line in dosya:
        if line != "\n":
            line_count += 1
    CreateToolTip(logGetir, text=f'{line_count} kayıt var.')
    dosya.close()

def cik():
    pencere.destroy()

def sifreCoz():
    dosya = open(yol, 'r').read()
    karakter = dosya.rstrip(' ').split(' ')
    for i in karakter:
        x = (chr(int(i)))
        print(x,end='')
def ac():
    subprocess.call(["gedit", yol])

pencere.title("Hourly Checker")

w = Label(pencere, text="Ne yapıyorsun", bg='SlateGray1', fg='RoyalBlue',font='Ubuntu 15 bold')
w.pack()

metin= Text(bg="SlateGray2", fg='RoyalBlue', height=10, width=44, font="Ubuntu 11 ")
metin.pack()

gonder= Button(text="Gönder", command=al, bg='SlateGray3', fg='SlateGray1',font='Ubuntu 12')
gonder.place(x=180, y=210)


cikis= Button(text="Çık", command=cik, bg='SlateGray3', fg='SlateGray4',font='Ubuntu 12 bold')
cikis.place(x=330, y=210)


logGetir = Button(text="Logları Getir", command=ac, bg='SlateGray3', fg='SlateGray1',font='Ubuntu 12')
logGetir.place(x=20, y=210)



pencere.mainloop()
exit('Son!')
